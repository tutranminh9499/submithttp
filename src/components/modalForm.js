import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from 'axios';
export default class ModalForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { modal: false,
      show: false,
      id:'',
      first_name: '',
      last_name:'' ,
      email: '',
      avatar: ''
    };

    this.toggle = this.toggle.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.updateState = this.updateState.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleChangeFirstName = this.handleChangeFirstName.bind(this);
    this.handleChangeLastName = this.handleChangeLastName.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangeAvatar = this.handleChangeAvatar.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleShow() {
    console.log(this.state)
    this.setState({ show: true })
}
handleClose(){
    this.setState({ show: false })
}
toggle() {
  this.setState({
    modal: !this.state.modal
  });
}
  updateState(id,first_name,last_name,email,avatar){
    this.setState({id: id});
    this.setState({first_name: first_name});
    this.setState({last_name: last_name});
    this.setState({email: email});
    this.setState({avatar: avatar});
    this.setState({
      modal: !this.state.modal
    });
  }

  
  handleChangeFirstName(event) {
    this.setState({first_name: event.target.value});
  }
  handleChangeLastName(event) {
    this.setState({last_name: event.target.value});
  }
  handleChangeEmail(event) {
    this.setState({email: event.target.value});
  }
  handleChangeAvatar(event) {
    this.setState({avatar: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    const Id=this.state.id;
    const user = {
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      avatar: this.state.avatar
    };

    axios.put(`http://localhost:3000/api/users/${Id}`, { user })
      .then(res => {
        console.log(res);
        console.log(res.data);
      })

      this.toggle();
     }


  render() {
    return (

        <div>
        <Modal isOpen={this.state.modal}>
        <form onSubmit={this.handleSubmit}>
    <ModalHeader>Update profile id {this.state.id}</ModalHeader>
          <ModalBody>
          <div className="row">
            <div className="form-group col-md-4">
            <label>First Name:</label>
            <input type="text" value={this.state.first_name} onChange={this.handleChangeFirstName} className="form-control" />
              </div>
              </div>
              <div className="row">
            <div className="form-group col-md-4">
            <label>Last Name:</label>
            <input type="text" value={this.state.last_name} onChange={this.handleChangeLastName} className="form-control" />
              </div>
              </div>
            <div className="row">
             <div className="form-group col-md-4">
            <label>Email:</label>
                <input type="text" value={this.state.email} onChange={this.handleChangeEmail} className="form-control" />
               </div>
              </div>
            <div className="row">
             <div className="form-group col-md-4">
              <label>Avatar</label>
                <input type="text" value={this.state.avatar} onChange={this.handleChangeAvatar} className="form-control" />
               </div>
              </div>
          </ModalBody>
          <ModalFooter>
            <input type="submit" value="Submit" onClick={() => this.props.updatePerson(this.state)} color="primary" className="btn btn-primary" />
            <Button color="danger" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
          </form>
        </Modal>
        </div>
      
    );
  }
}

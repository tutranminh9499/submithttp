import React from "react";
import {
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Row,
  Container,
  Breadcrumb,
  BreadcrumbItem,
  Jumbotron,
  Alert,
} from "reactstrap";
import axios from "axios";
import UserListCard from "./UserListCard.js";

export default class FormSubmit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // id: "",
      first_name: "",
      last_name: "",
      email: "",
      avatar:
        "https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png",
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }
  success() {
    alert("Success");
  }
  formAddRef = ({ createPerson }) => {
    this.addPerson = createPerson;
  };

  onAddClick = (person) => {
    this.addPerson(person);
    this.success();
  };

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }
  handleSubmit = (event) => {
    event.preventDefault();

    const user = this.state;
    // var newuser = this.state;

    axios.post(`http://localhost:3000/api/users`, { user }).then((res) => {
      this.setState({ id: res.data.id });
      console.log(res);
      console.log(res.data);
      // console.log(this.state);
      var newuser = this.state;
      this.onAddClick(newuser);
    });
    // setTimeout(() => {
    //   console.log("World!");
    // }, 2000);
    // console.log(this.state);
    // let newuser = this.state;
  };

  render() {
    return (
      <ul>
        <Jumbotron>
          <h1>HTTP REQUESTS</h1>
        </Jumbotron>
        <Container>
          <Breadcrumb>
            <BreadcrumbItem active>
              <h3>Add new user's profile:</h3>
            </BreadcrumbItem>
          </Breadcrumb>
          {/* <h2>Add new user's profile:</h2> */}
          <Row>
            <Col sm="20" md={{ size: 12, offset: 0 }}>
              <Form>
                <FormGroup row>
                  <Label for="examplename" sm={2}>
                    First Name
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="name"
                      name="first_name"
                      value={this.state.first_name}
                      onChange={this.handleInputChange}
                      id="examplename"
                      placeholder="Your first name"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="examplename" sm={2}>
                    Last Name
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="name"
                      name="last_name"
                      value={this.state.last_name}
                      onChange={this.handleInputChange}
                      id="examplename"
                      placeholder="Your last name"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleemail" sm={2}>
                    Email
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="email"
                      name="email"
                      value={this.state.email}
                      onChange={this.handleInputChange}
                      id="exampleemail"
                      placeholder="Your email address"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleText" sm={2}>
                    Description
                  </Label>
                  <Col sm={10}>
                    <Input type="textarea" name="text" id="exampleText" />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleemail" sm={2}>
                    Avatar Address
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="text"
                      name="avatar"
                      value={this.state.avatar}
                      onChange={this.handleInputChange}
                      id="exampleemail"
                      placeholder="Your avatar address"
                    />
                  </Col>
                </FormGroup>
                <FormGroup check row>
                  <div class="d-flex flex-row-reverse">
                    <Row>
                      <Col sm={{ size: 10, offset: 2 }}>
                        <a
                          type="submit"
                          onClick={this.handleSubmit}
                          class="btn btn-success"
                        >
                          Submit
                        </a>
                      </Col>
                    </Row>
                  </div>
                </FormGroup>
              </Form>
            </Col>
          </Row>
        </Container>
        <br></br>
        <Breadcrumb>
          <BreadcrumbItem active>
            <h3>USERS LIST</h3>
          </BreadcrumbItem>
        </Breadcrumb>
        <UserListCard ref={this.formAddRef} />
      </ul>
    );
  }
}

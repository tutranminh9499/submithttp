import React from "react";
import {
  Card,
  CardBody,
  Button,
  CardTitle,
  CardText,
  CardImg,
  CardSubtitle,
  Container,
  Row,
  Col,
  Form,
} from "reactstrap";
import axios from "axios";
import ModalForm from "./modalForm.js";
class UserListCard extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      persons: [],
    };
    this.createPerson = this.createPerson.bind(this);
    this.deletePerson = this.deletePerson.bind(this);
  }

  formModalRef = ({ updateState }) => {
    this.initData = updateState;
  };

  onUpdateClick = (id, first_name, last_name, email, avatar) => {
    this.initData(id, first_name, last_name, email, avatar);
  };

  createPerson(person) {
    // alert("success");
    console.log("test");
    console.log(person);
    this.setState({ persons: [...this.state.persons, person] });
  }

  deletePerson(Id) {
    this.handleRemove(Id);
    this.setState({
      persons: this.state.persons.filter((person) => person.id !== Id),
    });
  }

  updatePerson(newPerson) {
    const index = this.state.persons.findIndex((user) => {
      return user.id === newPerson.id;
    });
    this.state.persons[index] = newPerson;
    this.setState(this.state);
  }

  componentDidMount() {
    axios
      .get(`http://localhost:3000/api/users`)
      .then((res) => {
        const persons = res.data;
        this.setState({ persons });
      })
      .catch((error) => console.log(error));
  }

  handleRemove(Id) {
    const id = Id;
    axios.delete(`http://localhost:3000/api/users/${id}`).then((res) => {
      console.log(res);
      console.log(res.data);
    });
  }

  render() {
    const imgStyle = {
      Height: 300,
      Width: 300,
    };
    return (
      <ul>
        <ModalForm
          updatePerson={(newPerson) => this.updatePerson(newPerson)}
          ref={this.formModalRef}
        ></ModalForm>
        <Container>
          <Row>
            <Col sm="15" md={{ size: 10, offset: -1 }}>
              <div
                style={{
                  display: "grid",
                  gridTemplateColumns: "repeat(3, 1fr)",
                  gridGap: 20,
                }}
              >
                {this.state.persons.map((person) => (
                  <div key={person.id}>
                    <Container>
                      <Row>
                        {/* <Col sm="15" md={{ size: 5, offset: 3 }}> */}
                        <Col sm="17" md={{ size: 15 }}>
                          <Card>
                            <CardImg
                              top
                              width="100%"
                              src={person.avatar}
                              alt="Card image cap"
                              style={imgStyle}
                            />
                            <CardBody>
                              <CardTitle>
                                <h4 class="font-weight-bold">
                                  {person.first_name.toUpperCase()}{" "}
                                  {person.last_name.toUpperCase()}
                                </h4>
                              </CardTitle>
                              <CardSubtitle>
                                <h6 class="font-weight-bold">
                                  Email: {person.email}
                                </h6>
                              </CardSubtitle>
                              <CardText>
                                <p class="font-weight-bold">ID: {person.id}</p>
                              </CardText>
                              <div class="d-flex flex-row-reverse">
                                <div class="btn-group btn-group-justified">
                                  <a
                                    class="btn btn-primary"
                                    onClick={() =>
                                      this.onUpdateClick(
                                        person.id,
                                        person.first_name,
                                        person.last_name,
                                        person.email,
                                        person.avatar
                                      )
                                    }
                                  >
                                    Update
                                  </a>
                                  <a
                                    class="btn btn-danger"
                                    onClick={() => this.deletePerson(person.id)}
                                  >
                                    Delete
                                  </a>
                                </div>
                              </div>
                            </CardBody>
                          </Card>
                        </Col>
                      </Row>
                    </Container>
                  </div>
                ))}
              </div>
            </Col>
          </Row>
        </Container>
      </ul>
    );
  }
}
export default UserListCard;
